# Application name: CarCar

## Summary:
* Dealership Management System
![sides for main page](http://g.recordit.co/6DJHDTKKC0.gif)

## Video

<a href="https://www.youtube.com/embed/8x8rA-JkRac">Link to our CarCar application demo Video</a>


## Team:
* __Person 1__ - Lee Pham (Sales/Inventory)
* __Person 2__ - Eric Lu (Services/Inventory)

![image](/uploads/91c15294748db177ca424b89d358b465/image.png)

## Stack:
* HTML/CSS
* Python
* Django
* JavaScript
* React
* PostgreSQL
* Bootstrap
* RESTful APIs
* Docker
* Microservices 

## Port References:
* Services : 8080
* Sales : 8090
* Inventory : 8100
* React : 3000
* Database: PostgreSQL 

![Screen_Shot_2022-09-16_at_11.39.50_AM](/uploads/8d0026f04db573a737df6efdd1fcc008/Screen_Shot_2022-09-16_at_11.39.50_AM.png)

## Design:
* [RESTful API references](documentation/RESTful_apis/apis.md)
* [Models and attributes](documentation/modeling/data.md)


![UML_class_diagram_CarCar](/uploads/88928b8f6e0ec42f35b28bb7b925fbeb/UML_class_diagram_CarCar.png)

### Overview of CarCar Application
![image](/uploads/9cd71838323a75d90e699c668782bdcf/image.png)

## Directions for use:
##### Requirements: Docker desktop must be installed and running

###### 1. In your terminal, goto your projects folder and input the following commands
```
git clone https://gitlab.com/LeeNPham/project-beta
cd project-beta
docker volume create beta-data
docker-compose build
docker-compose up
```
###### 2. In your browser, go to http://localhost:3000/ to open the CarCar homepage.

###### 3. Enjoy exploring the Single Page Application to better familiarize yourself with our project and its capabilities


## MicroServices Capabilities:
Explanation of models and their integration with the inventory microservice

* #### Inventory capabilities:
   * A user can create a manufacturer(Make),a vehicle model(Model), and an automobile with a VIN number for a specific automobile to utilize within the system. 
   * A user can list all vehicle models including the manufacturer name, model name, and an image associated with the model. 
   * A user can list all automobiles currently in inventory, including details such as VIN, year, color, model, and manufacturer. 


* #### Sales capabilities:
   * A user may create/enroll a salesperson and then create/enroll a customer to associate the sale of an automobile with said customer.
   * A user may record a sale for a specific automobile with the price, by a specific sales person, for a particular customer. 
   * A user may see all the sales the dealership has done. 
   * A user may view the sales history of a specific sales person.


* #### Services capabilities:
  * A user is able to schedule an appointment for a vehicle by selecting VIN, VIP status, data, time, reason,and preferred technician for a customer. 
  * A user is able to check all upcoming appointments, and change the status of an appointment by clicking FINISHED or CANCEL button. 
  * A user is able to view Serivce history of a specific vehicle by selecting VIN and clicking SEARCH button in the search box area. 
  * A user is able to create a badge for a new technician by entering name and assigning an unnique technician employee ID. 

## Services microservice

The Services microservice mainly in charge of scheduling an appointment, checking the lift of all appointments, viewing a specific vehicle's service history at CarCar, and enrolling a new technican for services department. 

This services microservice needs to poll automobile data from inventory mircroservice by using VIN as unique identification to allow the data transferred. Automobile is a Value Object under services microservice. 

To schedule an appointment, a system user can select a vehicle (VIN) which is polled from inventory services for the appointment. The user needs to input the owner of the vehicle in order to allow the service department to contact with the customer for any updates about the appointment. VIP status can be selected with two options: YES or NOT YET. The user can manually input the date and time or select a date from a calendar by clicking the calendar icon. A dropdown menu for choosing an avaliable technician for the appointment. Lastly, the user can write down some notes for reason of the appointment such as, 30,000 miles maintainance, oil chnage, change air filters, etc. 

A list of all appointments will be shown with informantion of VIN, Owners Name, Reasons for Services, assigned Technician Name, Data/Time, and VIP status. Also two buttons: FINISHED / CANCEL will be used for each appointment. By clicking on FINISHED button, it will change the status of finish from False to True, then current window will be refreshed to show latest list of appointments. By clicking on Cancel button, it will change the status of cancel from False to True, then current window will be refreshed to show latest list of appointments. 

To track a specific vehicle's service history at CarCar, search box contains two parts: vin# selection box and search button. All VIN will be showed on the dropdown box, User can select one on the list. By clikcing the search button, A table of service history data for the selected vehicle will be displayed with table rows of VIN, Customer Name, Date, Time, Technician, and Reason.

## Sales microservice

The sales microservice requires us to be able to add a sales person, add a customer, record a sale, list all sales, and get a specific sales person's history. 
With these requirements, in order to do anything in sales, one would require automobile information from the Inventory API. Having automobile information allows me to create a sales record, list all sales, and get specific sales information for a sales person. 
Creating an Automobile value object model in the Sales application, we can then poll from the Inventory API for all of its automobile data and use it in the Sales microservice. 

Once we identify Inventory, Sales, and Services as the aggregate roots, we are able to better design our microservices in map them within their respective context boundaries. 

The Sales microservice, the AutomobileVO, SalesPerson, PotentialCustomer, and SalesRecord models were going to be the aggregates, because all of these in different combinations allow us to use methods on the Sales aggregate root, such as creating a sale, listing a sale, etc.

 SalesPerson and PotentialCustomer instead are seen as value entities. While they have similar properties such as name, they are not the same thing. 
 They have continuous identity due to how even though the salesperson and customer could change their name, they were still the same salesperson and the same customer. 
 SalesRecord would be identified as a value object due to how it has no life cycle. It simply exists with the recorded information.

On an ending note, we were able to utilize mostly bootstrap and other html/css related examples found throughout the web to provide a relatively passable Minimum Viable Product (MVP). The plan so far is that once the project requirements are completed and grading is completed, Eric and I will continue to update, upgrade, and provide more exciting and dynamic features to this SPA. Some things in the plans would include modals, more containers to minimize the number of visual distractions throughout the website, slides of our inventory images, image folders for each individual automobile, a very comprehensive list of attributes for the vehicle models, and a footer with links to various facets of a business entity such as it's mission, contact information, and so on.







