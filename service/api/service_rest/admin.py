from django.contrib import admin
from django.contrib import admin
from .models import Appointment, AutomobileVO, Tech

# Register your models here.
admin.site.register(AutomobileVO)

admin.site.register(Appointment)
admin.site.register(Tech)