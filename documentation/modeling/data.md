# Data model


## Manufacturer
| Name            | Type    | Unique | Optional |
|-----------------|---------|--------|----------|
| name            | str     | yes    | no       |

## VehicleModel
| Name            | Type    | Unique | Optional |
|-----------------|---------|--------|----------|
| name            | str     | no     | no       |
| picture_url     |media/url| no     | no       |
| manufacturer    | For.Key | yes    | no       |

## Automobile
| Name            | Type    | Unique | Optional |
|-----------------|---------|--------|----------|
| color           | str     | no     | no       |
| year            | int     | no     | no       |
| vin             | str     | yes    | no       |
| model           | For.Key | yes    | no       |

## AutomobileVO
| Name            | Type    | Unique | Optional |
|-----------------|---------|--------|----------|
| color           | str     | no     | no       |
| year            | int     | no     | no       |
| vin             | str     | yes    | no       |


## SalesPerson
| Name            | Type    | Unique | Optional |
|-----------------|---------|--------|----------|
| name            | str     | no     | no       |
| id              | int     | yes    | no       |

## PotentialCustomer
| Name            | Type    | Unique | Optional |
|-----------------|---------|--------|----------|
| name            | str     | no     | no       |
| address         | str     | no     | no       |
| phone           | int     | no     | no       |

## SalesRecord
| Name            | Type    | Unique | Optional |
|-----------------|---------|--------|----------|
| sales_person    | For.Key | yes    | no       |
| automobile      | For.Key | yes    | no       |
| customer        | For.Key | yes    | no       |
| price           | int     | no     | no       |

## Tech
| Name            | Type    | Unique | Optional |
|-----------------|---------|--------|----------|
| name            | str     | no     | no       |
| id              | int     | yes    | no       |

## Appointment
| Name            | Type    | Unique | Optional |
|-----------------|---------|--------|----------|
| tech            | For.Key | yes    | no       |
| owner           | str     | no     | no       |
| date            | DateTime| no     | no       |
| time            |TimeField| no     | no       |
| automobile      | For.Key | yes    | no       |
| reason          | str     | no     | no       |
| finished        | bool    | no     | no       |
| cancelled       | bool    | no     | no       |
| vip             | bool    | no     | no       |
