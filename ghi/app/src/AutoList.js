import React from 'react';

function AutoColumn(props) {
    return (
        <div className="col">
            {props.list.map(car => {
                return (
                    <div key={car.href} className="card mb-3 shadow">
                        <div className="card-body">
                        <ul className="list-unstyled">
                                <h2 className="card-title">VIN # {car.vin}</h2>
                                <ul>
                                    <li><strong>Year:</strong><p id="liData">{car.year}</p></li>
                                    <li><strong>Color:</strong><p id="liData">{car.color}</p></li>
                                    <li><strong>Model:</strong><p id="liData">{car.model.name}</p></li>
                                    <li><strong>Manufacturer:</strong><p id="liData">{car.model.manufacturer.name}</p></li>
                                </ul>
                            </ul>
                        </div>
                    </div>
                );
            })}
        </div>
    );
}


class AutoList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            carColumns: [[], []],
        };
    }


    async componentDidMount() {
        const url = 'http://localhost:8100/api/automobiles/';

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
        
                const requests = [];
                for (let car of data.autos) {
                    const detailUrl = `http://localhost:8100/api/automobiles/${car.vin}`;
        
                    requests.push(fetch(detailUrl));
                }
                const responses = await Promise.all(requests);
                const carColumns = [[], []];
                let i = 0;
                for (const carResponse of responses) {
                    if (carResponse.ok) {
                        const details = await carResponse.json();
                        carColumns[i].push(details);
                        i = i + 1;
                        if (i > 1) {
                            i = 0;
                        }
                    } else {
                        console.error(carResponse);
                    }
                }

                this.setState({ carColumns: carColumns });
            }
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        return (
            <>
                <div className="px-4 py-5 my-5 mt-0 text-center">
                    <img className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg" alt="" width="600" />
                    <h1 className="display-5 fw-bold">Automobiles</h1>
                    <div className="col-lg-6 mx-auto">
                        <p className="lead mb-4" id="vehModSub">
                            Take a look at our Automobile Database.
                        </p>
                    </div>
                </div>
                <div className="container" id="autoInfoContainer">
                    <h3 id="autoListScroll">Please scroll to see all options</h3>
                    <div className="row">
                        {this.state.carColumns.map((carList, index) => {
                            return (
                                <AutoColumn key={index} list={carList} />
                            );
                        })}
                    </div>
                </div>
            </>
        );
    }
}

export default AutoList;
