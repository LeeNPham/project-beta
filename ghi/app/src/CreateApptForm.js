import React from 'react';

class AppointmentForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            automobiles: [],
            owner: '',
            vip: '',
            date: '',
            time: '',
            techs: [],
            reason: ''
        };
        this.handleAutomobileChange = this.handleAutomobileChange.bind(this);
        this.handleOwnerChange = this.handleOwnerChange.bind(this);
        this.handleVipChange = this.handleVipChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleTimeChange = this.handleTimeChange.bind(this);
        this.handleTechChange = this.handleTechChange.bind(this);
        this.handleReasonChange = this.handleReasonChange.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleAutomobileChange(e) {
        const value = e.target.value;
        this.setState({automobile: value})
    }

    handleOwnerChange(e) {
        const value = e.target.value;
        this.setState({owner: value})
    }

    handleVipChange(e) {
        const value = e.target.value;
        this.setState({vip: value})
    }

    handleDateChange(e) {
        const value = e.target.value;
        this.setState({date: value})
    }

    handleTimeChange(e) {
        const value = e.target.value;
        this.setState({time: value})
    }

    handleTechChange(e) {
        const value = e.target.value;
        this.setState({tech: value})
    }

    handleReasonChange(e) {
        const value = e.target.value;
        this.setState({reason: value})
    }

    async handleSubmit(e) {
        e.preventDefault();
        const data = {...this.state, finished: "False", canceled: "False"};
        delete data.automobiles
        delete data.techs
        
        const appointmentUrl = `http://localhost:8080/api/appointment/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };
        // console.log("==========data is: ", data);
        const response = await fetch(appointmentUrl, fetchConfig);

        if (response.ok) {
            const newAppointment = await response.json();

            const cleared = {
                automobiles: [],
                owner: '',
                vip: '',
                date: '',
                time: '',
                techs: [],
                reason: ''
                };
            this.setState(cleared);
        }
    }



    async componentDidMount() {
        const techUrl = 'http://localhost:8080/api/tech/';
        const autoUrl = 'http://localhost:8100/api/automobiles/';
        
        const response = await fetch(techUrl);
        const autoResponse = await fetch(autoUrl);
        if (response.ok) {
            const techData = await response.json();
            // console.log("========techData:========",techData)
            const autoData = await autoResponse.json();
            // console.log("========autoData:========",autoData)
            this.setState({automobiles: autoData.autos})
            this.setState({techs: techData.tech})
        }
    }



    render() {
        return(
            <div className="row">
                    <div className="offset-3 col-6">
                        <div className="shadow p-4 mt-4">
                            <h1 id="appHeader">Create a new Appointment</h1>
                            <form onSubmit={this.handleSubmit} id="create-appt-form">
                                <div className="mb-3">
                                    <select onChange={this.handleAutomobileChange} value={this.state.automobile} required id="automobile" name="automobile" className="form-select">
                                        <option value="">Choose A Vehicle for Service</option>
                                        {this.state.automobiles.map(automobile => {
                                            return (
                                                <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                                            );
                                        })}
                                    </select>
                                </div> 
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleOwnerChange} value={this.state.owner} placeholder="Owner" required type="text" name="owner" id="owner" className="form-control" />
                                    <label htmlFor="owner">Customer Name</label>                     
                                </div>
                                <div className="form-floating mb-3">
                                    <select onChange={this.handleVipChange} value={this.state.vip} placeholder="Vip" required type="text" name="vip" id="vip" className="form-select" >
                                        <option value="">VIP Customer (Yes/No)</option>
                                        <option value="True">Yes</option>
                                        <option value="False">No(Not Yet)</option>
                                    </select>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleDateChange} value={this.state.date} placeholder="Date" required type="date" name="date" id="date" className="form-control" />
                                    <label htmlFor="date">Date MM/DD/YYYY</label>                     
                                </div>


                                <div className="form-floating mb-3">
                                    <input onChange={this.handleTimeChange} value={this.state.time} placeholder="Time" required type="time" name="time" id="time" className="form-control" />
                                    <label htmlFor="time">Time</label>                     
                                </div>


                                <div className="mb-3">
                                    <select onChange={this.handleTechChange} value={this.state.tech} required id="tech" name="tech" className="form-select">
                                        <option value="">Choose your Tech</option>
                                        {this.state.techs.map(tech => {
                                            return (
                                                <option key={tech.id} value={tech.id}>{tech.name}</option>
                                            );
                                        })}
                                    </select>
                                </div>       
                                <div className="form-floating mb-3">
                                    <input onChange={this.handleReasonChange} value={this.state.reason} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" />
                                    <label htmlFor="reason">Reason for Appointment</label>                     
                                </div>            
                                <button className="btn btn-primary" id="appBtn">Reserve the Appointment</button>
                            </form>
                        </div>
                    </div>
                </div>
        );
    }
}

export default AppointmentForm;
