import React from 'react';
import { Link } from 'react-router-dom';


function ManufacturerColumn(props) {
    return (
        <div className="col">
            {props.list.map(manufacturer => {
                return (
                    <div key={manufacturer.href} className="card mb-3 shadow">
                        <div className="card-body">
                            <h4 className="card-title">{manufacturer.name}</h4>
                        </div>
                    </div>
                );
            })}
        </div>
    );
}

class ManufacturerList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            manufacturerColumns: [[], [], []],
        };
    }

    async componentDidMount() {
        const url = 'http://localhost:8100/api/manufacturers/';

        try {
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();

                const requests = [];
                for (let manufacturer of data.manufacturers) {
                    const detailUrl = `http://localhost:8100${manufacturer.href}`;
                    requests.push(fetch(detailUrl));
                }

                const responses = await Promise.all(requests);

                const manufacturerColumns = [[], [], []];

                let i = 0;
                for (const manufacturerResponse of responses) {
                    if (manufacturerResponse.ok) {
                        const details = await manufacturerResponse.json();
                        manufacturerColumns[i].push(details);
                        i = i + 1;
                        if (i > 2) {
                            i = 0;
                        }
                    } else {
                        console.error(manufacturerResponse);
                    }
                }

                this.setState({ manufacturerColumns: manufacturerColumns });
            }
        } catch (e) {
            console.error(e);
        }
    }

    render() {
        return (
            <>
                <div className="px-4 py-5 my-5 mt-0 text-center">
                    <img className="bg-white rounded shadow d-block mx-auto mb-4" alt="" width="600" />
                    <h1 className="display-5 fw-bold">Manufacturers</h1>
                    <div className="col-lg-6 mx-auto">
                        <p className="lead mb-4" id="vehModSub">
                            Here's a list of manufacturers we service and sell!
                        </p>

                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        {this.state.manufacturerColumns.map((manufacturerList, index) => {
                            return (
                                <ManufacturerColumn key={index} list={manufacturerList} />
                            );
                        })}
                    </div>
                </div>
            </>
        );
    }
}

export default ManufacturerList;

