import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Nav from './Nav';
import MainPage from './MainPage';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import AutoList from './AutoList';
import AutoForm from './AutoForm';
import ModelList from './ModelList';
import ModelForm from './ModelForm';


import AddSalesPersonForm from './AddSalesPerson.js';
import AddCustomerForm from './AddCustomer.js';
import SaleRecordForm from './CreateSaleRecord.js';
import SaleList from './ListSales.js';
import SalesPersonHistory from './SalesPersonHistory.js';


import AddTechForm from './AddTechForm';
import AppointmentForm from './CreateApptForm';

import AppointmentList from './ListAppts';
import SearchList from './ServiceHistory';


function App(props) {
  return (
    <BrowserRouter>
      <header>
        <Nav />

        <div className="container">
          <Routes>
            <Route path="/" element={<MainPage />} />


          <Route path="/manufacturer" element={<ManufacturerList />} />
          <Route path="/createManufacturer" element={<ManufacturerForm />} />
          <Route path="/autoList" element={<AutoList />} />
          <Route path="/newAuto" element={<AutoForm />} />
          <Route path="/modelList" element={<ModelList />} />
          <Route path="/newModel" element={<ModelForm />} />


          <Route path="/addSalesPerson" element={<AddSalesPersonForm />} />
          <Route path="/addCustomer" element={<AddCustomerForm />} />
          <Route path="/recordSale" element={<SaleRecordForm />} />
          <Route path="/listSales" element={<SaleList />} />
          <Route path="/salespersonHistory" element={<SalesPersonHistory />} />

          <Route path="/addTech" element={<AddTechForm />} />
          <Route path="/newAppt" element={<AppointmentForm />} />

          <Route path="/appointmentList" element={<AppointmentList />} />
          <Route path="/searchList" element={<SearchList />} /> 
          </Routes>
        </div>
      </header>

    </BrowserRouter>
  );
}



export default App;
