import './index.css';

function MainPage() {
  return (
    <><div className="px-4 py-5 text-center" id="mainContainer">
      <h1 className="display-5 fw-bold" id="mainHeading">CarCar</h1>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4" id="slogan">
          The pinnacle solution for dealership management!
        </p>
      </div>
    </div>
    
    <div id="carouselExampleSlidesOnly" className="carousel slide" data-bs-ride="carousel">
        <div className="carousel-inner">
          <div className="carousel-item active">
            <img src="https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/mclaren720s.jpg?itok=af3JNEmf" className="d-block w-100" alt="slide1" />
          </div>
          <div className="carousel-item">
            <img src="https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/1-ferrari-sf90-stradale-2020-fd-hero-front_0.jpg?itok=mm3PNoWz" className="d-block w-100" alt="slide2" />
          </div>
          <div className="carousel-item">
            <img src="https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/1-maserati-mc20-2022-uk-first-drive-review-lead_1.jpg?itok=tfSL_zTL" className="d-block w-100" alt="slide3" />
          </div>
        </div>
      </div></>


  );
}

export default MainPage;
