from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)
    def __str__(self):
        return self.vin


class SalesPerson(models.Model):
    name = models.CharField(max_length=100)
    id = models.IntegerField(primary_key=True)
    def get_api_url(self):
        return reverse("api_show_salesperson_records", kwargs={"pk": self.pk})
    def __str__(self):
        return self.name


class PotentialCustomer(models.Model):
    name = models.CharField(max_length=100)
    address = models.TextField(max_length=300)
    phone = models.CharField(max_length=100)
    def get_api_url(self):
        return reverse("api_show_customer", kwargs={"pk": self.pk})
    def __str__(self):
        return self.name


class SalesRecord(models.Model):
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sales",
        on_delete=models.PROTECT
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.PROTECT
    )
    customer = models.ForeignKey(
        PotentialCustomer,
        related_name="sales",
        on_delete=models.PROTECT
    )
    price = models.IntegerField()

    def get_api_url(self):
        return reverse("api_show_sales", kwargs={"pk": self.pk})

    def __str__(self):
        return str("Salesperson: " + self.sales_person.name)
