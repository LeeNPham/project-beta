#Checklist for REACT:
##Backend:
* create app
* create DB Volume
* configure docker file and then docker compose
* configure app to installed apps
* configure CORS, DJWT2, Allowed_Hosts
* create models
* register models to admin
* create views for models and VOs of other api required attributes
* create encoders for views of models
* configure api paths for views of models
* create and configure poller for outside api polls
* test api paths using (CRUD)

##Frontend:
* install react create react-api 
* run npm start within virtualized docker container
* create CRUD js files (Written in JXL, based on the api functions created in api_views)
* lists js files (created with a mix of JXL and the api_list function within api_views)
* mainpage.js - to set up the default home webpage
* index.js - to add the app in strictmode for initial access
* app.js - to setup nav and the routes between each page
* nav.js - to setup the navlinks to the routes on the mainpage
* index.css - to style out the webpage (probably do this near the end when everything works already)
* readme.md - to disclose to the reader, how to run your project/webservice

## CarCar

### Team:
* __Person 1__ - Lee Pham (Sales)
* __Person 2__ - Eric Lu (Service)

#### Design
insert screenshot here
##### Shared steps for starting:
* In your terminal, goto your projects folder
```
git clone https://gitlab.com/LeeNPham/project-beta
cd project-beta
docker volume create beta-data
docker-compose build
docker-compose up
```

The starter application comes with a fully-functioning scaffold of microservices, a front-end application, and a database. The bold text services in the following list are the ones that you and your teammate will implement:

* Inventory API: provides Manufacturer, VehicleModel, and Automobile RESTful API endpoints but needs a front-end
  
* Database: the PostgreSQL database that will hold the data of all of the microservices
  
* React: the React-based front-end application where both you and your teammate will write the components to interact with your services **as well as the Inventory service
  
* Service API: the RESTful API to handle automobile service appointments
* Service Poller: a poller to use to integrate with other services
  
* Sales API: the RESTful API to handle sales information
* Sales Poller: a poller to use to integrate with other services

#### Divided responsibilities
* Show a list of manufacturers
* Create a manufacturer
* Show a list of vehicle models
* Create a vehicle model
* Show a list of automobiles in inventory
* Create an automobile in inventory

### Lee's additional notes:
* Inventory-api runs on port 8100
* Sales-api runs on port 8090
* Service-api runs on port 8080
* React-api runs through port 3000

### Lee's error logs:
Occured after creation of add potential customer, sales, and sales record
```
Compiled with problems:X
ERROR in ./src/App.js 9:0-51
Module not found: Error: Can't resolve './CreateSaleRecord.js' in '/app/src'

ERROR in ./src/App.js 10:0-38
Module not found: Error: Can't resolve './ListSales.js' in '/app/src'

ERROR in ./src/App.js 11:0-57
Module not found: Error: Can't resolve './SalesPersonHistory.js' in '/app/src'
```


### Lee's checklist:
* [x] Created readme template 
* [x] Added ```import dj_database_url``` to sales/api/sales_project/settings.py

* [x] __Created manufacturer model within Database (generate through Insomnia)__
*  Action	Method URL
     * [x] List manufacturers	GET	http://localhost:8100/api/manufacturers/
     * [x] Create a manufacturer	POST	http://localhost:8100/api/manufacturers/
     * [x] Get a specific manufacturer	GET	http://localhost:8100/api/manufacturers/:id/
     * [x] Update a specific manufacturer	PUT	http://localhost:8100/api/manufacturers/:id/
     * [x] Delete a specific manufacturer	DELETE	http://localhost:8100/api/manufacturers/:id/

* [x] __Created models model within Database (generate through Insomnia)__
*  Action	Method URL
     * [x] List vehicles	GET	http://localhost:8100/api/models/
     * [x] Create a vehicle	POST	http://localhost:8100/api/models/
     * [x] Get a specific vehicle	GET	http://localhost:8100/api/models/:id/
     * [x] Update a specific vehicle	PUT	http://localhost:8100/api/models/:id/
     * [x] Delete a specific vehicle	DELETE	http://localhost:8100/api/models/:id/

* [x] __Created automobile model within Database (generate through Insomnia)__
*  Action	Method URL
     * [x] List automobiles	GET	http://localhost:8100/api/automobiles/
     * [x] Create a automobile	POST	http://localhost:8100/api/automobiles/
     * [x] Get a specific automobile	GET	http://localhost:8100/api/automobiles/:vin/
     * [x] Update a specific automobile	PUT	http://localhost:8100/api/automobiles/:vin/
     * [x] Delete a specific automobile	DELETE	http://localhost:8100/api/automobiles/:vin/
* [x] Add CORS to installed apps and middleware to Sales settings.py
* [x] Add inventory-api to ALLOWED_HOST to Sales settings.py
* [x] Add api/ path for route to sales_rest.api_urls.py
* [x] Create models for Sales, include SalesPerson, PotentialCustomer, SalesRecord, 
* [x] Create encoders module for models to be combined later in views.py
* [x] Allow for encoders to match with views so that Json outputs can be provided through API urls later in Insomnia
  * [x] Encoders: AutomobileVOEncoder, SalesPersonEncoder, PotentialCustomerEncoder, SalesRecordEncoder

* [x] Create views for models and import both models and encoders
* [x] Configure Paths for api_urls
* [x] Add registration of models to admin.py

* [x] __Created SalesPerson model within Database (generate through Insomnia)__
*  Action	Method URL
     * [x] List SalesPersons	GET	http://localhost:8090/api/sales/person/
     * [x] Create a SalesPerson	POST	http://localhost:8090/api/sales/person/
     * [x] Get a specific SalesPerson	GET	http://localhost:8090/api/sales/person/:id/
     * [x] Update a specific SalesPerson	PUT	http://localhost:8090/api/sales/person/:id/
     * [x] Delete a specific SalesPerson	DELETE	http://localhost:8090/api/sales/person/:id/

* [x] __Created PotentialCustomer model within Database (generate through Insomnia)__
*  Action	Method URL
     * [x] List Customers	GET	http://localhost:8090/api/Customer/
     * [x] Create a Customer	POST	http://localhost:8090/api/Customer/
     * [x] Get a specific Customer	GET	http://localhost:8090/api/Customer/:id/
     * [x] Update a specific Customer	PUT	http://localhost:8090/api/Customer/:id/
     * [x] Delete a specific Customer	DELETE	http://localhost:8090/api/Customer/:id/

* [x] __Created SalesRecord model within Database (generate through Insomnia)__
*  Action	Method URL
     * [x] List sales	GET	http://localhost:8090/api/sales/
     * [x] Create a sale	POST	http://localhost:8090/api/sales/
     * [x] Get a specific sale	GET	http://localhost:8090/api/sales/:id/
     * [x] Update a specific sale	PUT	http://localhost:8090/api/sales/:id/
     * [x] Delete a specific sale	DELETE	http://localhost:8090/api/sales/:id/

* [x] Create a link in the navbar to get to the "Add a salesPerson" form 
  * [x] Create a AddSalesPerson.js form
* [x] Create a link in the navbar to get to the "Add a PotentialCustomer" form 
  * [x] Create a AddPotentialCustomer.js form
* [x] Create a link in the navbar to get to the "Create a SaleRecord" form 
  * [x] Create a CreateSaleRecord.js form
* [x] Edit MainPage.js to display compelling slogan
* [x] Add routes to app.js and index.js, maybe navlinks?
* [x] Re-added salesconfig to installed apps
* [x] Configured poller to provide information from other apis to sales api to generate sales
* [x] Tested and confirmed sales persons, customers apis in insomnia and they function
* [x] Tested and confirmed sales API access to db
* [x] I can't believe I just needed to restart docker so that my poller would work again T_T
* [x] I **Think** that I got the microservices for Sales up and running. I gotta do some testing.  
* [x] Work on navigation to I can see links to everything first! on the website ofc
* [ ] Test to see if add customer form works
* [ ] Test to see if add sales person form works
* [ ] Test to see if add sales record works
* [ ] Test to see if I can see a sales persons History
* [ ] Update CSS from google templates and their apis
* [ ] Update images for template design
* [ ] 
* [ ] 
* [ ] Consider putting in status for automobile to show it's no longer in inventory? 
* [ ] Onsubmit through sales form for status of car within inventory to change from available to sell to not available 

### Eric's checklist:
#### Planning
* Day1 Mo: Planning, designing, setting up RESTful API for services
* Day2 Tu: React Components
* Day3 We: React Components cont... All features should be functional by EOD
* Day4 Th: Testing feacures/users experiences/adding more CSS
* Day5 Fr: Wrap up README.md to finialize the project
#### Initializing and Designing milestone
* [x]  Intial git commit testing  
* [x]  Setting up API in isomnia for Manufacturers, Vehicle Models, and Automobile information
* [x] Model designing
* [x] Microservice diagram

#### VS code Backend coding milestone
* [x] Createing model
* [x] Views, urls
  * [x] project urls, app urls
* [x] Encoders for autoVO, Tech, Appt
* [x] Register app
* [x] Initial migrations
* [x] Migrate and create superuser the serivce app.
  * [x] admin:admin localhost:8080/admin/
  * [x] login via admin page and double checked models are registered.
* [x] A VIN is composed of 17 characters (digits and capital letters) that act as a unique identifier for the vehicle.
* [x] RESTful API for Service Microservice.
* [x] Compeleted insomnia endpoint for RESTful
* [x] Poller, polling data to serivce microservice. 
* [x] rebuiild docker-compose every time poller.py is motified/adjusted.
* [x] making sure the allowhost for data transferring.
  * [x] ALLOWED HOSTS has been set up
    ``` 
    ALLOWED_HOSTS = [
    "localhost",
    "inventory-api"]

* [x] testing out the data
  * [x] Object of type date is not JSON 
  * [x] Resolved: BLOCKAGE, need to figure this out. potential solution, adding encoder to date. will work on this on Day 2. serializable when creating appt. 
  * [x] Add magic__str__for better humna read in admin site.  
  * [x] Merge branch to main before working on frontend
  * [x] Making sure re-build docker compose, after sales microservice is merged. (poller.py is motified)

#### React frontend coding milestone
* [x] draw diagram for API pages demo
  * [x] inventory 
  * [x] sale
  * [x] service
* [x] Create tech form. 
  * [x] Submit URL http://localhost:8080/api/tech/
  * [x] Form design
    * Input field 1 : Tech name  --- name
    * Input filed 2 : employee ID --- id
    * Button : submit
* [x] Create appt form. 
  * [x] Submit URL http://localhost:8080/api/appointment/
  * [x] form design
    * Select field 1: automobile (VIN) dropdown options http://localhost:8100/api/automobiles/
    * Input field 2: owner name
    * Input field 3: VIP status (T/F)
    * Input field 4: Date
    * Input field 5: Time
    * Select field 6: assigned tech (tech ID) dropdown options http://localhost:8080/api/tech/
    * Input field 7: reasons?
    * Button: Submit
  
Create a Vehicle model Form design
* [x] Form post to : http://localhost:8100/api/models/
  * [x] input field 1: Name
  * [x] input field 2: picture_url
  * [x] selection field 3: manufacturers from http://localhost:8100/api/manufacturers/
  * [x] button: Create


Create auto Form design
* [x] Form post to http://localhost:8100/api/automobiles/
  * [x] Selection field 1: Models from http://localhost:8100/api/models/
  * [x] input field 2: Year
  * [x] input field 3: color
  * [x] input field 4: VIN
  * [x] button: Create

* [x] List of appts
  * [x] Fetch data http://localhost:8080/api/appointment/ 
  * [x] Finish Button onclick refresh the window to get latest list of appts
  * [x] Cancel buttion onclick refresh the window to get latest list of appts


* [ ] Service hisotry for a specific VIN
  * [x] Search bar, allowing user to type in the VIN # and search for hisotry
  * [x] Adding a page title float in the middle 
  * [x] search buttion on click event
  * [ ] intinally show all service history?? optional-- better to have


## Inventory frontend componants
LEE:
* [x] 1. List of Manufacturers
* [x] 2. Create a manufacturer
* [x] 3. List of Vehicle Models

ERIC

* [x] 1.  Create a Vehicle model
* [x] 2.  List of Autos
* [x] 3.  Create an Auto




## Ports Use:
Services : 8080

Sales : 8090

Inventory : 8100

React : 3000

Database: 15432



## Service microservice

Explain your models and integration with the inventory
microservice, here.

## Sales microservice

Explain your models and integration with the inventory
microservice, here.


## Video Demo


<figure class="video_container">
  <iframe src="https://recordit.co/CI841kIqRp" frameborder="0" allowfullscreen="true"> </iframe>
</figure>


